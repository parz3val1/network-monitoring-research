from scapy.all import sniff, Packet
from scapy.layers.dns import DNS
import datetime
from typing import Dict


def get_domains(pkt: Packet) -> Dict:
    """
    takes in packets and returns the response domains.
    parm1: Packet (scapy packet)
    Note: the program will fail if it doesn't find DNS in the packet
    """
    try:
        hostname = pkt[DNS].qd.qname
        return {f"{str(datetime.datetime.now())}": hostname.decode('utf-8')}

    except KeyboardInterrupt:
        return {0: 0}


def main() -> None:
    """
    Sniffs for destination port 53 for the UDP packets.
    UDP because really easy to capture.
    But it doesn't have proper filtering for only requests.
    Can also provide the dictionary of domain and timestamps.
    """
    print(sniff(filter="udp and dst port 53", prn=get_domains, store=0))


if __name__ == "__main__":
    main()
