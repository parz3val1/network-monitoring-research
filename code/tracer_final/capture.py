# Methods to capture the network packets with scapy

import typing
from scapy.all import sniff, wrpcap, PacketList
from scapy.config import conf
import time
from typing import Dict, IO
import tempfile

# Setting the configuration of the scapy to use the libpcap
# Libpcap is faster and improves the performance of the scapy scanner
conf.use_pcap = True


def packet_capture_with_file_str(packet_count: int, filename: str) -> None:
    """
    Captures packet file into the directory the script is loaded in.
    Needs : Packet count, and file_name
    """
    p = sniff(count=packet_count)
    with open(filename, "wb") as cap_file:
        wrpcap(cap_file, p, append=True)


def packet_capture_with_temp_file(packet_count: int) -> IO:
    """
    Captures packets and returns a temporaryfile IO object.
    The file is temporary and is immediately deleted after its closed.
    Named Temp file is used because just using the temp file without suffix
    will cause the scapy wrcap to fail
    """
    # open an IO object with the extension in temp directory
    capfile = tempfile.NamedTemporaryFile(mode="w+b", suffix=".cap", delete=False)
    p: PacketList = sniff(count=packet_count)

    # writing to a pkt, closes _file object
    # don't pass anything you don't want to close
    # the binary needs to be opened with CM and file.name will give the name
    wrpcap(capfile, p, append=True)
    return capfile
