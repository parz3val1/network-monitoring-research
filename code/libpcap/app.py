import pcap

sniffer = pcap.pcap(name=None, promisc=True, immediate=True, timeout_ms=50)



address = lambda pkt, offset: '.'.join(str(pkt[i]) for i in range(offset, offset + 4))

for ts, pkt in sniffer:
    print(f"{(ts, address(pkt, sniffer.dloff + 12))}\tSRC")
    print(f"{(ts, address(pkt, sniffer.dloff + 16))}\tDST")