from starlette.applications import Starlette
from starlette.responses import JSONResponse
import json
from domain_capture import threading_loop
from multiprocessing import Process


# Start a starlette app
app = Starlette(debug=True)


async def scanner_process():
    # Create a new process with the threading loop
    new_process = Process(target=threading_loop)

    # Start the background scanner process
    new_process.start()


@app.route("/", methods=["GET"])
def homepage(request):
    return JSONResponse({"tracer": "track user activity"})


@app.route("/history/", methods=["GET"])
async def history(request):
    await scanner_process()
    with open("history.json", "r") as json_file:
        data = json.load(json_file)
        return JSONResponse(data)
