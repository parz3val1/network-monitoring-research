import pyshark
import dpkt
import time
import os

file_name = "captured.cap"
packetlen = 1024

# capture sequence
capture_script = f"tshark -q -F pcap -c {packetlen} -w {file_name}"
os.system(capture_script)

# wait before the analysis step
time.sleep(0.5)

# analysis sequence
cap_file = open(file_name, "rb")
pcap_r = dpkt.pcap.Reader(cap_file)

for ts, buf in pcap_r:
    # get IP traffic
    try:
        eth = dpkt.ethernet.Ethernet(buf)
    except:
        continue

    if eth.type != 2048:
        continue

    try:
        ip = eth.data
    except:
        continue

    # filter udp assigned ports for DNS
    try:
        udp = ip.data
    except:
        continue

    # for the DNS traffic
    try:
        dns = dpkt.dns.DNS(udp.data)
    except:
        continue

    for qname in dns.qd:
        print(qname.name)
