# Basic Research

## Requirements:

Track and sniff the packets
Find the routes, ports and IP for the outbound and inbound connections

### Background Research and Overview:

#### Intro: OSI Layer: Intro Logical division of networking technologies and processes

Source: `https://www.youtube.com/watch?v=HEEnLZV2wGI&ab_channel=ElitheComputerGuy`

    >>7 layers

- Application Layer(Firefox, IE and more) >> This is where the User Lives

- Presentation Layer ( the layer OS work in ) >> Operating system level, drivers and security keys and stuff.

  // This is where our sniffer could live

- Session Layer (Layer that deals with creating session between two systems) >> Creating, destroying sessions between two systems

- Transport Layer ( Decides how much information is sent at one time) >> ((Windowing, send information back and forth, how big block is sent, or how long to wait.))

- Network Layer (The layers routers operate at, your IP address) >> This is where things like IP goes here. Anything with TCP/IP goes here.

  //Another suitable place for a sniffer - Reading a few pages of paper tells me that it's the best place to do the forensics.

- Data link Layer (switches, operators and stuff) >> Mac Address (Media Access control), every device has a unique address. ((ARP - Address Resolution Protocol))

  //packets get too unrecognizable to monitor

- The Physical Layer (All the physical stuff, wires, panels and cords and more) >>
  > > 95% of all networking problems are layer 1 problems.

**Packet sniffing intro:** a process of monitoring and capturing all the packets passing through a given network using sniff tools.

- Things that can be sniffed:
  - Email Traffic (we need)
  - FTP Passwords (we don’t need)
  - Web Traffic (we need)
  - Telnet passwords (we don’t need)
  - Router configuration (don’t need)
  - Chat sessions (don’t need)
  - DNS traffic (only inference)

**How does the sniffing work?**

- the NIC of the sys is changed to monitor mode (think of airmon-ng) and listens to all the data transmitted on its segment.
  By default, NIC ignores all the traffic not addressed to it. (But in our case, if we are monitoring the user’s device instead of entire network activity, traffic will be addressed to the NIC of the user, meaning will we be able to monitor the traffic without starting the interface in monitor mode? )
  Types of sniffing
  Passive - traffic is locked but is not altered. Only allows listening and works with hub devices. Because the most modern devices use switches and don’t use hub devices. Passive sniffing is almost ineffective these days.
  Active sniffing- traffic is not just locked and monitored, but also altered in some way by an attacker, and is used to sniff a switch based network. It involves injecting multiple ARP packets into a target network to flood on the switch CAM(content addressable memory) table.
  Types of active sniffing - MAC flooding - DHCP attacks - DNS Poisoning - Spoofing Attacks - ARP Poisoning
- The modern networking infrastructure is aware of when it is being tampered with. So, sniffing exerts effects on some protocols except TCP/IP, and they don’t offer any resistance to potential intruders.

**Some protocols that allow some level of sniffing are:**

- HTTP : No encryption, easy and real target
- SMTP - efficient but no protection against sniffing
- NNTP (Network News Transfer Protocol) - used for all types of network communication, drawback is data and even passwords are sent as clear text.
- POP - no protection against sniffing and can be trapped
- FTP - no security features and can be sniffed.
- IMAP - same as SMTP but very vulnerable to sniffing
- Telnet - Sends everything over as clear text. Bad

**But what about HTTPS?**

> > Attacker cannot see anything past the domain,

Source: Is it possible to sniff HTTPS URLs?

`https://security.stackexchange.com/questions/176164/is-it-possible-to-sniff-https-urls/176167`

- What can the sniffer see from HTTPS?
  If ‘S’ is sniffer and ‘U’ is user and ‘S’ is not running on ‘U’s PC then, ‘S’ can see - U is making web request with HTTPS to 203.0.113.98 - Destination port is 443, which is obvious. - If ‘S’ does ‘rDNS’ lookup, it can find out IP is used for example.com - If ‘S’ looks at SNI (Server Name Indication), it is actually foo.example.com

_More search needed: Ethernet Segment / Frame_ 🖼

_Note: Difference between monitoring packets in/out of the device and in the network._

**Good paper on network analysis to read beforehand:**

Packet analysis for network forensics: A comprehensive survey

Source:
`https://www.sciencedirect.com/science/article/pii/S1742287619302002`

The paper provides the brief introduction, tools available, and the comprehensive walkthrough and benchmarking of the available methodologies. The best bet of finding the right method for our requirements.

Deep packet inspection
AI-powered packet analysis methods with advanced network traffic classification and pattern identification capabilities
In-network forensics

### Network sniffing libraries in Python:

> > libpcap : low level library for network forensics, originally written in C (pcap). Used by one of the most famous analysis tools. (tcpdump)
- very hard to find documentation in Python and the library is wrapped in python with CTypes. I need some knowledge of ctypes before I can use this library. Highly highly performant.
Docs : https://www.tcpdump.org/manpages/ 
- the documentataion for the library is in C, but the methods are available in Python as well through ctypes.

> > pypcap - Pypcap is the OO wrapper around the library libpcap
- offers a wrapper around the libpcap but is slightly easier and has some documentation. Also offers good performance. 
- https://pypcap.readthedocs.io/en/latest/ 


> > scapy - packet manipulation library for the python. Supports, attacking, sniffing, fingerprinting, scanning and decoding packets. 
